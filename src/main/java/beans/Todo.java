package beans;

public class Todo {
  private int id;
  private String title;
  private int userId;

  // 引数なしのコンストラクタ
  public Todo() {
  }

  public Todo(int id, String title, int user_id) {
    super();
    this.id = id;
    this.title = title;
    this.userId = user_id;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int user_id) {
    this.userId = user_id;
  }

}
