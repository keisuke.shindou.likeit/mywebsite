package beans;

import java.io.Serializable;

public class User implements Serializable {
  private int id;
  private String name;
  private String email;
  private String passwor;

  // 引数なしのコンストラクタ
  public User() {

  }

  // コンストラクタ生成
  public User(int id, String name, String email, String passwor) {
    super();
    this.id = id;
    this.name = name;
    this.email = email;
    this.passwor = passwor;
  }

  // アクセサの生成
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPasswor() {
    return passwor;
  }

  public void setPasswor(String passwor) {
    this.passwor = passwor;
  }
}
