package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Todo;
import dao.TodoDao;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  // TodoDaoクラスのtitleAllメソッドを呼び出す。
      TodoDao todoDao = new TodoDao();
      List<Todo> todoList = todoDao.titleAll();
	    // UserDaoクラスのfindAllメソッドの戻り値(userList)をセットする
      request.setAttribute("todoList", todoList);
	    // userList.japにフォワードをする
	    // ユーザー一覧画面を表示
      RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/list.jsp");
	    dispatchar.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
