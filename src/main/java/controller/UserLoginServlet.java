package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      RequestDispatcher dispachar = request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
      dispachar.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// submit(ログインボタン)が押された際にdoPostが起動
      // 文字化け防止
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String email = request.getParameter("email");
      String password = request.getParameter("password");

      // UserDaoクラスのfindByLoginメソッドを呼び出す
      UserDao userDao = new UserDao();
      // 入力されたパスワードを暗号化
      String encorderdPassword = PasswordEncorder.encordPassword(password);
      User user = userDao.findByLoginInfo(email, encorderdPassword);

      // テーブルに該当のデータが見つからなかった場合
      if (user == null) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "メールアドレスまたはパスワードが異なります。");
        // 入力したログインIDを画面に表示するため、リクエストに値をセット
        request.setAttribute("email", email);
        // ログインjspにフォワード("errMsg"と"emailを保存")
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);
        return;
      }
   
      // セッションスコープにユーザの情報をセット
      //ユーザ情報をセット
      HttpSession session = request.getSession();
      session.setAttribute("user", user);
      // タイトル一覧のサーブレットにリダイレクト
      response.sendRedirect("ListServlet");
    }



	}


