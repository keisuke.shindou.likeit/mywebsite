package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Todo;
import dao.TodoDao;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  // idをgetParameterメソッドで取得
		  //"UpdateServlet?○○=の合わせる。
	      int id = Integer.valueOf(request.getParameter("id"));
	      // UserDaoクラスのfindIdメソッドを呼び出す
	      TodoDao todoDao = new TodoDao();	      
	      Todo todo = todoDao.findById(id);
	      //update.jsoのhidden　value="${○○.id}に合わせる。
	      request.setAttribute("todo", todo);
		 // update.jspの画面の表示
		 RequestDispatcher dispachar = request.getRequestDispatcher("WEB-INF/jsp/update.jsp");
	      dispachar.forward(request, response);
		}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  // submit(更新ボタン)が押された際にdoPostが起動
	      // 文字化け防止
	      request.setCharacterEncoding("UTF-8");
	      // 入力された値をgetParameterで取得
	      int id = Integer.parseInt(request.getParameter("id"));
	      String title = request.getParameter("title");
	      //TodoDaoのupdateを呼び出す
	      TodoDao todoDao = new TodoDao();
	      // タイトル名が未入力の場合
	      if (title.equals("")) {
	    	// リクエストスコープにエラーメッセージをセット
		    Todo todo = todoDao.findById(id);
		    todo.setTitle(title);
	        // リクエストスコープにエラーメッセージをセット
	        request.setAttribute("errMsg", "入力された内容は正しくありません");
	        request.setAttribute("todo", todo);
	        // ログインjspにフォワード("errMsg"と"loginIdとnameとbirthDateを保存")
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
	        dispatcher.forward(request, response);
	        return;
	      }
	      todoDao.update(title, id);
	      //ListServletへリダイレクト
	      response.sendRedirect("ListServlet");

	}

	}

