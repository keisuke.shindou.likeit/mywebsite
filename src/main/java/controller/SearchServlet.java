package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Todo;
import dao.TodoDao;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// submit(検索ボタン)が押された際にdoPostが起動
	    // 文字化け防止
	    request.setCharacterEncoding("UTF-8");
	    // フォームの値を受け取る。入力されたタイトル名を取得
	    // jspのformで指定したname="○○"と合わせる
	    String title = request.getParameter("title");
        TodoDao todoDao = new TodoDao();
         //リクエストスコープにタイトル名をセット
         request.setAttribute("title",title);
         List<Todo> titleSerch = todoDao.serch(title);
         //TodoDaoクラスのserchメソッドの戻り値(titleList)をセットする
         request.setAttribute("todoList", titleSerch);
          //list.japにフォワードをする
         RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/list.jsp");
         dispatchar.forward(request, response);

	}

}
