package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TodoDao;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // creat.jsｐの表示
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
      dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // 追加ボタンが押されたら起動
      // 文字化け防止
      request.setCharacterEncoding("UTF-8");
      // フォームの値を受け取る。
      // jspのformで指定したname="○○"と合わせる
      String title = request.getParameter("title");
      // TodoDaoのcreateを呼び出す。
      TodoDao todoDao = new TodoDao();
   // タイトル名が未入力の場合
      if (title.equals("")) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        // ログインjspにフォワード("errMsg"と"loginIdとnameとbirthDateを保存")
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
        dispatcher.forward(request, response);
        return;
      } 
      todoDao.create(title);
      // タイトル一覧外面へリダイレクト
      response.sendRedirect("ListServlet");


	}

}
