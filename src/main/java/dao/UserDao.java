package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import beans.User;

public class UserDao {
  public User findByLoginInfo(String email, String password) {
    Connection conn = null;
    try {
      // データベースへ接続する
      conn = DBManager.getConnection();
      // SELECT文を準備する
      String sql = "SELECT * FROM user WHERE email = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 検索結果は１件のみなのでif文を使用
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name");
      String _email = rs.getString("email");
      String _password = rs.getString("password");
      
      return new User(id, name, _email, _password);

    } catch (SQLException e) {

      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}
