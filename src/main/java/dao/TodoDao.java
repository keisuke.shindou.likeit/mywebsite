package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Todo;

public class TodoDao {
public List<Todo> titleAll(){
  
  Connection conn = null;
  List<Todo> todoList = new ArrayList<Todo>();
  try {
    // データベースへ接続
    conn = DBManager.getConnection();
    // セレクト文のの準備（findAllでは管理者を除くSQLにすること)）
    String sql = "SELECT * FROM todo ORDER BY id DESC";

    // セレクト文の結果を取得し表示する
    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery(sql);
    // 結果表に格納されたレコード内容を
      while (rs.next()) {
      int id = rs.getInt("id");
      String title = rs.getString("title");
      int userId = rs.getInt("user_id");
      Todo todo = new Todo(id, title,userId);
      todoList.add(todo);
    }
  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベースの切断する
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }

  }
  return todoList;
}



// 新規登録を行う。
public void create(String title) {
  Connection conn = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続する
    conn = DBManager.getConnection();
    // INSERT文を準備する
    String sql = "INSERT INTO todo (user_id,title) VALUES (1,?)";

    // INSERT文を実行し、結果表を取得
    pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, title);
    // SQLの実行
    pStmt.executeUpdate();

  } catch (SQLException e) {
    e.printStackTrace();
    return;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return;
      }
    }
  }
}


//タイトルの更新を行う
public void update(String title , int id) {
 Connection conn = null;
 PreparedStatement pStmt = null;
 try {
   // データベースへ接続する
   conn = DBManager.getConnection();
   // UPDATE文を準備する
   String sql =
       "UPDATE todo SET title = ? WHERE id = ?";
   // UPDATE文を実行し、結果表を取得
   pStmt = conn.prepareStatement(sql);
   pStmt.setString(1, title);
   pStmt.setInt(2, id);
   // SQLの実行
   pStmt.executeUpdate();
 } catch (SQLException e) {
   e.printStackTrace();
   return;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return;
     }

}
 }
}

//ユーザーの削除に関する処理
public void delete(int id) {
 Connection conn = null;
 PreparedStatement pStmt = null;
 try {
   // データベースへ接続する
   conn = DBManager.getConnection();
   // DELETE文を準備する
   String sql = "DELETE FROM todo WHERE id = ?";
   // SELECTを実行し、結果表を取得
   pStmt = conn.prepareStatement(sql);
   pStmt.setInt(1, id);
   // SQLの実行
   pStmt.executeUpdate();
 } catch (SQLException e) {
   e.printStackTrace();
   return;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return;
     }

}
 }
}

//タイトル名の詳細
public Todo findById(int id) {
    Connection conn = null;
    try {
        // データベースへ接続する
        conn = DBManager.getConnection();
        // SELECT文を準備する
        String sql = "SELECT * FROM todo WHERE id = ?";
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);
        // SQLの実行
        ResultSet rs = pStmt.executeQuery();
        // 検索結果は１件のみなのでif文を使用
        if (!rs.next()) {
          return null;
        }
        int _id = rs.getInt("id");
        String title = rs.getString("title");
        int userId = rs.getInt("user_id");
        return new Todo(_id, title,userId);

      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
      }
    }


//タイトル名の検索（部分一致検索可能）
public List<Todo> serch (String title) {
	Connection conn = null;
    List<Todo> todoList = new ArrayList<Todo>();
    PreparedStatement pStmt = null;
	try {
		// データベースへ接続
	      conn = DBManager.getConnection();
	      // セレクト文のの準備
	      String sql = "SELECT * FROM todo WHERE title LIKE ? ORDER BY id DESC";
	      pStmt = conn.prepareStatement(sql);
	      pStmt.setString(1, "%" + title + "%");
	      // SQLの実行
	        ResultSet rs = pStmt.executeQuery();
	   // 結果表に格納されたレコード内容を
	      while (rs.next()) {
	      int id = rs.getInt("id");
	      String _title = rs.getString("title");
	      int userId = rs.getInt("user_id");
	      Todo todo = new Todo(id, _title,userId);
	      todoList.add(todo);
	    }
	  } catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	  } finally {
	    // データベースの切断する
	    if (conn != null) {
	      try {
	        conn.close();
	      } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	      }
	    }

	  }
	  return todoList;
	}
	}

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
