CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
USE mywebsite;
INSERT INTO todo (user_id,title) VALUES (1,'勉強する')

-- userテーブル --

CREATE TABLE user (
 id SERIAL PRIMARY KEY,
 name varchar(255) NOT NULL,
 email varchar(255) UNIQUE NOT NULL,
 password varchar(255) NOT NULL
 );

INSERT INTO user (name,email,password)
            VALUES ('新堂奎佑','keisuke.shindou.likeit@gmail.com','password')
UPDATE user SET password=MD5('password') WHERE id=1;
-- todoテーブル --

CREATE TABLE todo (
 id SERIAL PRIMARY KEY,
 title varchar(255) NOT NULL,
 user_id int REFERENCES user(id)
 );
 
SELECT * FROM user WHERE email = 'keisuke.shindou.likeit@gmail.com' and password = 'password'; 
INSERT INTO todo (user_id,title) VALUES (1,'勉強する');
INSERT INTO todo (user_id,title) VALUES (1,'何かをする');
UPDATE todo SET title = '起きる' WHERE id = 4 and user_id = 1;

DELETE FROM todo WHERE id = 5;
SELECT * FROM todo WHERE id = 10;
SELECT * FROM todo WHERE title LIKE '%す%' ORDER BY id DESC;

DROP TABLE todo;
